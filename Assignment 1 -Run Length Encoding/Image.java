import java.io.*;
import java.util.*;
import java.awt.*;
import java.awt.image.*;
import javax.imageio.*;

// This class represents a simple rectangular image, where each pixel can be
// one of 16 colours.
public class Image {

    // Store a 2 dimensional image with "colours" as numbers between 0 and 15
    private int pixels[][];

    // Read in an image from a file. Each line of the file must be the same
    // length, and only contain single digit hex numbers 0-9 and a-f.
    public Image(String filename) {

        // Read the whole file into lines
        ArrayList<String> lines = new ArrayList<String>();
        try {
            BufferedReader in = new BufferedReader(new FileReader(filename));
            for (String s = in.readLine(); s != null; s = in.readLine())
                lines.add(s);
        }
        catch (FileNotFoundException e) {
            System.out.println("File not found: " + filename);
            System.exit(1);
        }
        catch (IOException e) {
            System.exit(2);
        }

        if (lines.size() == 0) {
            System.out.println("Empty file: " + filename);
            System.exit(1);
        }

        // Initialise the array based on the number of lines and the length of the
        // first one.
        int length = lines.get(0).length();
        pixels = new int[lines.size()][length];

        for (int i = 0; i < lines.size(); i++) {
            // Check that all of the lines have the same length as the first one.
            if (length != lines.get(i).length()) {
                System.out.println("Inconsistent line lengths: " + length + " and " + lines.get(i).length() + " on lines 1 and " + (i+1));
                System.exit(1);
            }

            // Copy each line into the array
            for (int j = 0; j < length; j++) {
                pixels[i][j] = Character.getNumericValue(lines.get(i).charAt(j));
                if (pixels[i][j] < 0 || pixels[i][j] > 15) {
                    System.out.println("Invalid contents: " + lines.get(i).charAt(j) + " on line " + (i+1));
                    System.exit(1);
                }
            }
        }
    }

    // Create a solid image with given dimensions and colour
    public Image(int height, int width, int colour) {
        pixels = new int[height][width];
        for (int i = 0; i < height; i++)
            for (int j = 0; j < width; j++)
                pixels[i][j] = colour;
    }

    // Get back the original text-based representation
    public String toString() {
        StringBuilder s = new StringBuilder(pixels.length * pixels[0].length);
        for (int i = 0; i < pixels.length; i++) {
            for (int j = 0; j < pixels[i].length; j++)
                s.append(Integer.toHexString(pixels[i][j]));
            s.append("\n");
        }
        return s.toString();
    }

 // Taken from the lecture by Owens, S. (2017). RLE Java.
  // Available at: https://moodle.kent.ac.uk/2017/pluginfile.php/264777/mod_resource/content/1/RLE.java
  // Accessed 27/10/2017.

public static int[] encode(int[] data) {
    ArrayList<Integer> encoded = new ArrayList<Integer>();
    int i = 0;
    while(i < data.length) {
      int current = data[i];
      int runLength = 0;
      // Count how long the next run is
      while(i < data.length && data[i] == current) {
        i++;
        runLength++;
      }
      encoded.add(runLength);
      encoded.add(current);
    }
    // Have the ArrayList into an array
    int[] encoded2 = new int[encoded.size()];
    for(i = 0; i < encoded.size(); i++) {
      encoded2[i] = encoded.get(i);
    }
    return encoded2;
  }


    // TASK 2: Implement the compress method to create and return a list of
    // drawing commands that will draw this image.
    // 6 marks for correctness -- does the command list exactly produce the
    // input image.
    // 5 marks for being able to shrink test-image1 and test-image2 into no more
    // than 7 commands each. You can work out these commands by hand, but the
    // marks here are for your implemented algorithm (HINT: think Run-length
    // Encoding) being able to create the commands.
    // 4 marks for shrinking the other, more difficult, test images. We'll run
    // this as a competition and give all 4 to the best 20% of the class, 3 to
    // the next best 20%, and so on.
    public Drawing compress() {

     int counter = 0;
     Drawing imgCommands = new Drawing(pixels.length, pixels[0].length, 0);
  
        imgCommands.addCommand(new DrawingCommand("up 0 " + (Integer.toHexString(pixels[0][0]))));
        counter++;
    
        for(int posY = 0; posY < pixels.length; posY++){
            for(int posX = 0; posX < pixels[0].length; posX++){ 
                if(posX != 0) {
                    imgCommands.addCommand(new DrawingCommand("right 1 " + (Integer.toHexString(pixels[posY][posX]))));
                    counter++;
                }

                if(posX == pixels[0].length - 1) {
                    imgCommands.addCommand(new DrawingCommand("left " + (pixels[0].length - 1)));
                    counter++;

                    if(posY + 1 < pixels.length){
                    imgCommands.addCommand(new DrawingCommand("down 1 " + (Integer.toHexString(pixels[posY+1][0]))));
                    counter++;
                    }
                }
            }
        }

        return imgCommands;
        }

    // This is the standard 4-bit EGA colour scheme, where the numbers represent
    // 24-bit RGB colours.
    static int[] colours =
          { 0x000000, 0x0000AA, 0x00AA00, 0x00AAAA,
            0xAA0000, 0xAA00AA, 0xAA5500, 0xAAAAAA,
            0x555555, 0x5555FF, 0x55FF55, 0x55FFFF,
            0xFF5555, 0xFF55FF, 0xFFFF55, 0xFFFFFF};

    // Render the image into a PNG with the given filename.
    public void toPNG(String filename) {

        BufferedImage im = new BufferedImage(pixels[0].length, pixels.length, BufferedImage.TYPE_INT_RGB);

        for (int i = 0; i < pixels.length; i++)
            for (int j = 0; j < pixels[i].length; j++) {
                im.setRGB(j,i,colours[pixels[i][j]]);
            }

        File f = new File(filename + ".png");
        try {
            ImageIO.write(im,"PNG",f);
        }
        catch (IOException e) {
            System.out.println("Unable to write image");
            System.exit(1);
        }
    }


    public void setPixels(int row, int col, int con) {
        pixels[row][col] = con;
    }

    public static void main(String[] args) {
        // A simple test to read in an image and print it out.
        Image i = new Image(args [0]);  
        System.out.print(i.toString());
        i.toPNG(args[0]); 

        Drawing d = i.compress();
        System.out.println(d);

}
}



